bool anyUserUnder18(Iterable<User> users) {
  return users.any((user) => user.age < 18);
}

bool everyUserOver13(Iterable<User> users) {
  return users.every((user) => user.age > 13);
}

Iterable<User> filterOutUnder21(Iterable<User> users) {
  return users.where((user) => user.age >= 21);
}

Iterable<User> findShortNamed(Iterable<User> users) {
  return users.where((user) => user.name.length <= 3);
}

Iterable<String> getNameAndAge(Iterable<User> users) {
  return users.map((e) => '${e.name} is ${e.age}');
}

void main(List<String> args) {
  var users = [
    User(name: 'ABCDE', age: 14),
    User(name: 'BCDEF', age: 18),
    User(name: 'CDE', age: 18),
    User(name: 'DEF', age: 21),
    User(name: 'E', age: 25),
  ];

  if (anyUserUnder18(users)) {
    print('Have user under 18');
  }
  if (everyUserOver13(users)) {
    print('Have user over 13');
  }

  var AgeMoreThan21Users = filterOutUnder21(users);
  for (var user in AgeMoreThan21Users) {
    print(user);
  }
  var shortNamedUsers = findShortNamed(users);
  for (var user in shortNamedUsers) {
    print(user);
  }

  var nameAndAge = getNameAndAge(users);
  for (var user in nameAndAge) {
    print(user);
  }
}

class User {
  String name;
  int age;
  User({required this.name, required this.age});
  @override
  String toString() {
    return '$name, $age';
  }
}
